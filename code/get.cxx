#include "get.h"

using namespace std;

TH1 *GetHist(TString fichier, int tableNumber, int centrality)
{

    TFile *f = new TFile(fichier);

    // cout<<"f="<<f<<endl;
    TH1 *hist;

    TString nameHist = "Hist1D_y";
    TString nameTable = "Table ";

    nameHist += centrality;
    nameTable += tableNumber;

    TH1 *e1, *e2, *e3;
    e1 = (TH1 *)f->GetDirectory(nameTable)->Get(nameHist + "_e1");
    e2 = (TH1 *)f->GetDirectory(nameTable)->Get(nameHist + "_e2");
    e3 = (TH1 *)f->GetDirectory(nameTable)->Get(nameHist + "_e3");

    hist = (TH1 *)f->GetDirectory(nameTable)->Get(nameHist)->Clone();

    for (int i = 0; i < hist->GetNbinsX(); i++)
    {
        if (e3 != NULL)
            hist->SetBinError(i, TMath::Sqrt(TMath::Power(e1->GetBinContent(i), 2) + TMath::Power(e2->GetBinContent(i), 2) + TMath::Power(e3->GetBinContent(i), 2)));
        else
            hist->SetBinError(i, TMath::Sqrt(TMath::Power(e1->GetBinContent(i), 2) + TMath::Power(e2->GetBinContent(i), 2)));
    }

    delete e1;
    delete e2;
    if (e3 != NULL)
        delete e3;

    // delete f;

    return hist;
}

TGraph *GetGraph(TString fichier, int tableNumber, int centrality)
{
    TFile *f = new TFile(fichier);
    TGraph *g;
    TString nameTable = "Table ", nameGraph = "Graph1D_y";
    nameTable += tableNumber;
    nameGraph += centrality;

    g = (TGraph *)f->GetDirectory(nameTable)->Get(nameGraph)->Clone();

    delete f;
    return g;
}

Double_t GetPartmass(TString fichier, int tableNumber)
{
    double_t mass = 0;
    if (fichier == "data/HEPData-1569102768-v1-root.root" || fichier == "HEPData-1569102768-v1-root.root")
    {
        switch (tableNumber)
        {
        case 1:
        case 2:
            mass = 0.139; //[GeV/c] pions
            break;
        case 3:
        case 4:
            mass = 0.493; //[GeV/c] kaons
            break;
        case 5:
        case 6:
            mass = 0.938; ////[GeV/c] protons
        default:
            break;
        }
    }
    else if (fichier == "data/HEPData-ins1762368-v1-root.root" || fichier == "HEPData-ins1762368-v1-root.root")
    {
        switch (tableNumber)
        {
        case 1:
        case 2:
            mass = 1.425; //[GeV/c] Koaon* 0
            break;
        case 3:
        case 4:
            mass = 1.019; //[GeV/c] meson phi
            break;
        default:
            break;
        }
    }
    return mass;
}

Double_t GetNormBW( int tableNumber, int centrality)
{
   //result of the normalisation obtained with combined fit 

   double_t N1[10] = { 567152, 552244 ,  357689, 221492, 121901,  58514.8, 30372.1, 10391.2, 2853.79, 1110.02}; //protons
   double_t N2[10] = { 18410.2, 20064.1, 16000.7, 12086.3,  9264.74, 6453.78, 4421.12 , 2524.24,  1111.83 , 600.919}; //pions
   double_t N3[10] = {39944, 40989.5, 29880.7, 20571.8,  13557.5, 7942.99, 4712.19, 2120.75 , 745.031 ,340.664 }; //kaons

    switch (tableNumber)
    {
    case 1://pions
        return N2[centrality-1];  
        break;

    case 3: //kaons
        return N3[centrality-1];
        break;

    case 5://protons
        return N1[centrality-1];
        break;

    default:
        return 0;
        break;
    }

}

Double_t GetRadius(int centrality)
{
    double_t R[10] = {10.5, 8.8, 8.1, 7.3, 6.2, 5.3, 4.5, 3.6, 3.1, 2.3};
    return R[centrality - 1];
}
