
#include <TROOT.h>
#include <TObject.h>
#include <TMath.h>

using namespace std;

Double_t DistrExp(Double_t *x, Double_t *par) //id_dis = 1
{
    //works for low p_t
    // x[0] : p_T
    // T : temperature
    Double_t mass = par[0]; //mass [GeV/c]
    Double_t T = par[1];    //Temperature [GeV/c]
    Double_t N = par[2];    //Yield dN/dy

    Double_t m_T = TMath::Sqrt(x[0] * x[0] + pow(mass, 2)); //transverse mass

    return N * (x[0] / (T * (mass + T))) * exp((-m_T + mass) / T);
}

Double_t DistrBoltzmann(Double_t *x, Double_t *par) //id_dis = 2
{

    Double_t mass = par[0]; //mass [GeV/c]
    Double_t T = par[1];    //Temperature [GeV/c]
    Double_t C = par[2];    //Normalisation factor

    Double_t m_T = TMath::Sqrt(x[0] * x[0] + pow(mass, 2)); //transverse mass

    return C * x[0] * exp(-(m_T - mass) / T);
}

Double_t DistrLevy(Double_t *x, Double_t *par) //id_dis = 3
{
    Double_t mass = par[0]; //mass [GeV/c]
    Double_t T = par[1];    //Temperature [GeV/c]
    Double_t N = par[2];    //Yield dN/dy
    Double_t n = par[3];    //power index

    Double_t m_T = TMath::Sqrt(x[0] * x[0] + pow(mass, 2)); //transverse mass

    return N * x[0] * (n - 1) * (n - 2) / ((n * T) * (n * T + (mass * (n - 2)))) * pow(1 + (m_T - mass) / (n * T), -n);
}

Double_t DistrPow(Double_t *x, Double_t *par) //id_dis = 4
{
    //which one to use?

    Double_t p_T_avg = par[0]; //average momentum [GeV/c]
    Double_t N = par[1];       //Yield dN/dy
    Double_t n = par[2];       //power index

    return N * 4 * (n - 1) * (n - 2) * x[0] / ((n - 3) * (n - 3) * p_T_avg * p_T_avg) * pow(1 + x[0] / (p_T_avg * (n - 3) * 0.5), -n);
}

TString GetdistName(Int_t id_dis)
{
    TString name;

    switch (id_dis)
    {
    case 1:
        name = name.Append("DistrExp");
        break;
    case 2:
        name = name.Append("DistrBoltzmann");
        break;
    case 3:
        name = name.Append("DistrLevy");
        break;
    case 4:
        name = name.Append("DistrPow");
        break;
    default:
        name = name.Append("all");
        break;
    }

    return name;
}

TGraph *GetGraph(TString fichier, int tableNumber, int centrality)
{
    TFile *f = new TFile(fichier);
    TGraph *g = new TGraph();
    TString nameTable = "Table ", nameGraph = "Graph1D_y";
    nameTable += tableNumber;
    nameGraph += centrality;

    g = (TGraph *)f->GetDirectory(nameTable)->Get(nameGraph)->Clone();

    delete f;
    return g;
}

Double_t GetPartmass(TString fichier, int tableNumber){
    double_t mass = 0;
    if (fichier == "data/HEPData-1569102768-v1-root.root" || fichier ==   "HEPData-1569102768-v1-root.root" )
    {
        switch (tableNumber)
        {
        case 1:
        case 2:
            mass = 0.139; //[GeV/c] pions
            break;
        case 3:
        case 4:
            mass = 0.493; //[GeV/c] kaons
            break;
        case 5:
        case 6:
            mass = 0.938; ////[GeV/c] protons
        default:
            break;
        }
    }
    else if (fichier == "data/HEPData-ins1762368-v1-root.root" || fichier == "HEPData-ins1762368-v1-root.root")
    {
        switch (tableNumber)
        {
        case 1:
        case 2:
            mass = 1.425; //[GeV/c] Koaon* 0
            break;
        case 3:
        case 4:
            mass = 1.019; //[GeV/c] meson phi
            break;
        default:
            break;
        }
    }
    return mass;
    

}

void distributionfit(Int_t id_dis, TString fichier, int tableNumber, int centrality, TString name)
{

    TGraphAsymmErrors *graph = (TGraphAsymmErrors *)GetGraph(fichier, tableNumber, centrality);
    graph->SetTitle("p_T distribtion of p+ pbar in Pb-Pb collision");
    graph->GetXaxis()->SetLimits(0, 10);
    graph->SetLineColor(65);

    //Draw
    TCanvas *c1 = new TCanvas("distribution" + GetdistName(id_dis), "Fit", 80, 80, 900, 900);
    c1->Print("./histo/fit/" + name + "/fit" + GetdistName(id_dis) + name + ".pdf[");
    // Inside this canvas, we create two pads

    switch (id_dis)
    {
    case 1:
    {
        gStyle->SetOptFit(0011);
        //Exponential distribution
        TF1 *f1 = new TF1("Exp", DistrExp, 0, 10, 3); //3 parameters
        f1->SetParameters(GetPartmass(fichier, tableNumber), 0.427, 74);
        f1->FixParameter(0, GetPartmass(fichier, tableNumber));
        f1->SetParNames("mass", "Temperature", "Yield");
        f1->SetLineColor(2); //rouge
        f1->SetRange(0, 10);
        graph->Fit("Exp", "EX", "", 0, 2);
        f1->Draw();
        graph->Draw("sames");
        c1->Update();
        c1->Print("./histo/fit/" + name + "/fit" + GetdistName(id_dis) + name + ".pdf");
        cout << "first bin = " << graph->GetPointX(0) << endl;
        cout << "Exp Integral = " << f1->Integral(graph->GetPointX(0), 10) << endl;
        cout << "Total Exp Integral = " << f1->Integral(0, 10) << endl;
        cout << "Extrapolated Exp Integral = " << f1->Integral(0, 10) - f1->Integral(graph->GetPointX(0), 10) << endl;
    }
    break;
    case 2:
    {
        gStyle->SetOptFit(0011);
        //Boltzmann distribution
        TF1 *f2 = new TF1("Boltzmann", DistrBoltzmann, 0, 10, 3); //3 parameters
        f2->SetParameters(GetPartmass(fichier, tableNumber), 0.1, 1);
        f2->SetParLimits(1, 0, 0.9);
        f2->FixParameter(0, GetPartmass(fichier, tableNumber));
        f2->SetParNames("mass", "Temperature", "Normalisation");
        f2->SetLineColor(4); //bleu
        f2->SetRange(0, 10);
        graph->Fit("Boltzmann", "EX0", "", 0, 2);
        f2->Draw();
        graph->Draw("sames");
        c1->Update();
        c1->Print("./histo/fit/" + name + "/fit" + GetdistName(id_dis) + name + ".pdf");
        cout << "Boltzmann Integral = " << f2->Integral(graph->GetPointX(0), 10) << endl;
        cout << "Total Boltzmann Integral = " << f2->Integral(0, 10) << endl;
        cout << "Extrapolated Boltzmann Integral = " << f2->Integral(0, 10) - f2->Integral(graph->GetPointX(0), 10) << endl;
    }
    break;
    case 3:
    {
        gStyle->SetOptFit(0011);
        //Levy distribution
        TF1 *f3 = new TF1("Levy", DistrLevy, 0, 10, 4);
        f3->SetParameters(GetPartmass(fichier, tableNumber), 0.164, 74, 4);
        f3->FixParameter(0, GetPartmass(fichier, tableNumber));
        f3->SetParLimits(1, 0, 0.5);
        f3->SetParNames("mass", "Temperature", "Yield ", "Power index");
        f3->SetLineColor(6); //magenta
        f3->SetRange(0, 10);
        graph->Fit("Levy", "EX0", "", 1, 4);
        f3->Draw();
        graph->Draw("sames");
        c1->Update();
        c1->Print("./histo/fit/" + name + "/fit" + GetdistName(id_dis) + name + ".pdf");
        cout << "Levy Integral = " << f3->Integral(graph->GetPointX(0), 10) << endl;
        cout << "Total Levy Integral = " << f3->Integral(0, 10) << endl;
        cout << "Extrapolated Levy Integral = " << f3->Integral(0, 10) - f3->Integral(graph->GetPointX(0), 10) << endl;
    }
    break;
    case 4:
    {
        gStyle->SetOptFit(0011);
        //Power distribution
        TF1 *f4 = new TF1("Power", DistrPow, 0, 10, 3);
        f4->SetParameters(1.44, 74, 5);
        f4->SetParNames("average momentum", "Yield ", "Power index");
        f4->SetLineColor(9); //violet
        f4->SetRange(0, 10);
        graph->Fit("Power", "EX0", "", 0.4, 4);
        f4->Draw();
        graph->Draw("sames");
        c1->Update();
        c1->Print("./histo/fit/" + name + "/fit" + GetdistName(id_dis) + name + ".pdf");
        cout << "Power Integral = " << f4->Integral(graph->GetPointX(0), 10) << endl;
        cout << "Total Power Integral = " << f4->Integral(0, 10) << endl;
        cout << "Extrapolated Power Integral = " << f4->Integral(0, 10) - f4->Integral(graph->GetPointX(0), 10) << endl;
    }
    break;

    default:
        //Exponential distribution
        TF1 *f1 = new TF1("Exp", DistrExp, 0, 10, 3); //3 parameters
        //f1->SetMaximum(45);
        f1->SetTitle("fit of p_T distribtion of " + name);
        f1->SetParameters(GetPartmass(fichier, tableNumber), 0.427, 74);
        f1->FixParameter(0, GetPartmass(fichier, tableNumber));
        f1->SetParNames("mass", "Temperature", "Yield");
        f1->SetLineColor(2); //rouge
        f1->SetRange(0, 10);
        graph->Fit("Exp", "EX0", "", 0, 2);
        f1->Draw();
        graph->Draw("sames");
        c1->Update();
        //Boltzmann distribution
        TF1 *f2 = new TF1("Boltzmann", DistrBoltzmann, 0, 10, 3); //3 parameters
        f2->SetParameters(GetPartmass(fichier, tableNumber), 0.1, 1);
        f2->FixParameter(0, GetPartmass(fichier, tableNumber));
        f2->SetParNames("mass", "Temperature", "Normalisation");
        f2->SetLineColor(4); //bleu
        f2->SetRange(0, 10);
        graph->Fit("Boltzmann", "EX0", "", 0, 2);
        f2->Draw("sames");
        graph->Draw("sames");
        c1->Update();
        //Levy distribution
        TF1 *f3 = new TF1("Levy", DistrLevy, 0, 10, 4);
        f3->SetParameters(GetPartmass(fichier, tableNumber), 0.164, 74, 4);
        f3->FixParameter(0, GetPartmass(fichier, tableNumber));
        f3->SetParLimits(1, 0, 0.9);
        f3->SetParNames("mass", "Temperature", "Yield ", "Power index");
        f3->SetLineColor(6); //magenta
        f3->SetRange(0, 10);
        graph->Fit("Levy", "EX0", "", 1, 4);
        f3->Draw("sames");
        graph->Draw("sames");
        c1->Update();
        //Power distribution
        TF1 *f4 = new TF1("Power", DistrPow, 0.5, 10, 3);
        f4->SetParameters(1.44, 74, 5);
        f4->SetParLimits(2, 0, 100);
        f4->SetParLimits(1, 0, 100);
        f4->SetParNames("average momentum", "Yield ", "Power index");
        f4->SetLineColor(9); //violet
        f4->SetRange(0, 10);
        graph->Fit("Power", "EX0", "", 0.4 , 4 );
        f4->Draw("sames");
        graph->Draw("sames");
        c1->Update();
        c1->Print("./histo/fit/" + name + "/fit" + GetdistName(id_dis) + ".pdf");
        break;
    }
    c1->SaveAs("./histo/fit/" + name + "/fit" + GetdistName(id_dis) + name + ".root");
    c1->Print("./histo/fit/" + name + "/fit" + GetdistName(id_dis) + name + ".pdf]");
}

void testfit()
{
    /*
    //Pb-Pb : p-pbar
    for (int i = 0; i < 5; i++)
    {
        distributionfit(i, "data/HEPData-1569102768-v1-root.root", 5, 1, "PbPb_ppbar");
    } 

    */

    
    //p-p : p-pbar
    for (int i = 0; i < 5; i++)
    {
        distributionfit(i, "data/HEPData-1569102768-v1-root.root", 6, 1, "pp_ppbar");
    }

    /*

    //Pb-Pb : phi
    for (int i = 1; i < 5; i++)
    {
        distributionfit(i, "data/HEPData-ins1762368-v1-root.root", 3, 1, "PbPb_phi");
    }

    
    
    //p-p : phi
    for (int i = 0; i < 5; i++)
    {
        distributionfit(i, "data/HEPData-ins1762368-v1-root.root", 4, 1, "pp_phi");
    }
    */
}