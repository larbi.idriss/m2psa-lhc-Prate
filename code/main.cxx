// O.Poncet and I.Larbi
//__________________________________________________________________________
// main function to obtained the results of the report
//
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TMultiGraph.h>
#include <TGraph.h>
#include "distributionfit.h"
#include "contourfit.h"
#include "combinedfit.h"

using namespace std;

int main()
{
    gROOT->ProcessLine("gErrorIgnoreLevel = 3001;");
    gROOT->ProcessLine("gPrintViaErrorHandler = kTRUE;");

    /*
     // ************************************************** Pb-Pb : p-pbar
     //BW and contour
     //centrality = 1 for the more central collision and 10 for peripheral collisions
     for (int i = 1; i <= 10; i++)
     {
         distributionfit(5, "data/HEPData-1569102768-v1-root.root", 5, i, "PbPb_ppbar", 0.3, 3); // BW
     }
*/
     contours("PbPb_ppbar", 1, 10);
/*

     //Best value of the yeild
     distributionfit(1, "data/HEPData-1569102768-v1-root.root", 5, 1, "PbPb_ppbar", 0, 3); //exp
     distributionfit(2, "data/HEPData-1569102768-v1-root.root", 5, 1, "PbPb_ppbar", 0, 1.2); //Boltzmann
     distributionfit(3, "data/HEPData-1569102768-v1-root.root", 5, 1, "PbPb_ppbar", 0.55, 20); //Levy
     distributionfit(4, "data/HEPData-1569102768-v1-root.root", 5, 1, "PbPb_ppbar", 0.49, 20); //Power
     distributionfit(5, "data/HEPData-1569102768-v1-root.root", 5, 1, "PbPb_ppbar", 0.3, 3); //Power

     */

    /*

    // ************************************************** p-p : p-pbar
    //Best value of the yeild
    distributionfit(1, "data/HEPData-1569102768-v1-root.root", 6, 1, "pp_ppbar", 0, 1.2); //exp
    distributionfit(2, "data/HEPData-1569102768-v1-root.root", 6, 1, "pp_ppbar", 0, 1.2); //Boltzmann
    distributionfit(3, "data/HEPData-1569102768-v1-root.root", 6, 1, "pp_ppbar", 0.3, 20); //Levy
    distributionfit(4, "data/HEPData-1569102768-v1-root.root", 6, 1, "pp_ppbar", 0, 0); //Power

    distributionfit(5, "data/HEPData-1569102768-v1-root.root", 6, 1, "pp_ppbar", 0.3, 2); //BW

    // **************************************************Pb-Pb : phi
    distributionfit(1, "data/HEPData-ins1762368-v1-root.root", 3, 1, "PbPb_phi", 0, 1.2); //exp
    distributionfit(2, "data/HEPData-ins1762368-v1-root.root", 3, 1, "PbPb_phi", 0, 1.2); //Boltzmann
    distributionfit(3, "data/HEPData-ins1762368-v1-root.root", 3, 1, "PbPb_phi",0.3, 20); //Levy
    distributionfit(4, "data/HEPData-ins1762368-v1-root.root", 3, 1, "PbPb_phi", 0.49, 20); //Power

    //BW and contour
    for (int i = 1; i <= 8; i++)
    {
        distributionfit(5, "data/HEPData-ins1762368-v1-root.root", 3, i, "PbPb_phi", 0.3, 3); // BW
    }

    contours("PbPb_phi", 1, 8);


    // **************************************************p-p : phi
    for (int i = 0; i < 5; i++)
    {
        distributionfit(i, "data/HEPData-ins1762368-v1-root.root", 4, 1, "pp_phi", 0.3, 3);
    }



     // **************************************************Pb-Pb : pions
    //BW and contour
    for (int i = 1; i <= 10; i++)
    {
        distributionfit(5, "data/HEPData-1569102768-v1-root.root", 1, i, "PbPb_pions", 0.3, 3); // BW
    }

    contours("PbPb_pions", 1, 10);

    // **************************************************Pb-Pb : kaons
    //BW and contour
    for (int i = 1; i <= 10; i++)
    {
        distributionfit(5, "data/HEPData-1569102768-v1-root.root", 3, i, "PbPb_kaons", 0.3, 3); // BW
    }

    contours("PbPb_kaons", 1, 10);


    // distributionfit(5, "data/HEPData-1569102768-v1-root.root", 3, 1, "PbPb_kaons", 0.3, 3); // BW
*/

    // combinedFit(1);

    /*
    for (int i = 1; i <= 10; i++)
    {
        combinedFit(i); // BW
    }
    
    distributionfit(5, "data/HEPData-1569102768-v1-root.root", 5, 1, "PbPb_ppbar", 0.3, 3); // protons
    distributionfit(5, "data/HEPData-1569102768-v1-root.root", 1, 1, "PbPb_pions", 0.5, 1.5); // pions
    distributionfit(5, "data/HEPData-1569102768-v1-root.root", 3, 1, "PbPb_kaons", 0.2, 1.5); // kaons
*/
    gROOT->ProcessLine("gErrorIgnoreLevel = -1;");
    gROOT->ProcessLine("gPrintViaErrorHandler = kFALSE;");

    return 0;
}
