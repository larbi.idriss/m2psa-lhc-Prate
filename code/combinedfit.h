//O.Poncet and I.Larbi 
//__________________________________________________________________________
// Function to fit 3 graph in the same time 

#ifndef COMBINED_FIT
#define COMBINED_FIT
#include <TROOT.h>
#include <TObject.h>
#include <TMath.h>
#include <TString.h>
#include <TGraphAsymmErrors.h>
#include <TCanvas.h>
#include <TF1.h>
#include <TStyle.h>
#include <TPaveStats.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TMinuit.h>
#include <TVirtualFitter.h>
#include <TMultiGraph.h>
#include <TGraph.h>
#include <TLine.h>
#include <Fit/Fitter.h>
#include <Fit/BinData.h>
#include <Fit/Chi2FCN.h>
#include <Math/WrappedMultiTF1.h>
#include <HFitInterface.h>
#include <TCanvas.h>
#include <TStyle.h>
#include "distributionfit.h"

struct GlobalChi2;//definition of the global chi2 structure containing the information of 3 species with commun parameters like T or <B_t> and individual parameter for each set of data for example each species has its own normalisation 
void combinedFit(int centrality); //Perfoms a multifit using Blast-Wave distribution over Kaons pions and protons in Pb-Pb collisions for a given centrality

#endif
