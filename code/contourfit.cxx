#include "contourfit.h"

using namespace std;

void contourfit(TF1 *f, TString name, TString centralitystr, Int_t id_dis, Int_t par1, Int_t par2)
// Draw the contours of the given paramaters for ERRDEF=1 in first Pad and ERRDEF=1 and 2 in second Pad
{
    //TFile *file = new TFile("./histo/fit/" + name + "/contour/contour" + centralitystr +"gr.root", "recreate"); //for combinedfit
    TFile *file = new TFile("./histo/fit/" + name + "/" + centralitystr + "/contour/contour" + GetdistName(id_dis) + name + "gr.root", "recreate");

    TCanvas *c2 = new TCanvas("c2", "contours", 10, 10, 600, 800);
    c2->cd(1);
    auto mg = new TMultiGraph();
    // get contour for ERRDEF=1 for parameter 1 versus parameter 2
    gMinuit->SetErrorDef(4);
    TGraph *gr12 = (TGraph *)gMinuit->Contour(200, par1, par2);
    gr12->SetTitle(name + " contour " + GetdistName(id_dis));
    gr12->GetXaxis()->SetTitle(f->GetParName(par1));
    gr12->GetYaxis()->SetTitle(f->GetParName(par2));
    mg->Add(gr12);
    // gr12->Draw("alp");
    gr12->Write("contour1");

    // get center
    auto gr = new TGraph();
    gr->SetPoint(0, f->GetParameter(par1), f->GetParameter(par2));
    gr->SetMarkerStyle(kPlus);
    mg->Add(gr);
    // gr->Draw("lp");
    gr->Write("center");

    mg->SetTitle(name + " contour " + GetdistName(id_dis));
    mg->GetXaxis()->SetTitle(f->GetParName(par1));
    mg->GetYaxis()->SetTitle(f->GetParName(par2));
    mg->Draw("apl");
    
    c2->SaveAs("./histo/fit/" + name + "/" + centralitystr + "/contour/contour" + GetdistName(id_dis) + name + ".root");
    //c2->SaveAs("./histo/fit/" + name + "/contour/contour" + centralitystr +".root"); //for combinedfit 
    file->Close();
}

void contours(TString name, Int_t centralitymin, Int_t centralitymax)
// Draw the contours for given datas and centralities
{

    auto c = new TCanvas("c", "multigraph", 800, 600);
    c->SetGrid();

    auto mg = new TMultiGraph();
    TString centralitystr = ""; // centrality value converted to TString

    for (int i = centralitymin; i <= centralitymax; i++)
    {
        centralitystr += i;
        TFile *f1 = new TFile("./histo/fit/" + name + "/" + centralitystr + "/contour/contourBW" + name + "gr.root");
        TGraph *g1 = (TGraph *)f1->Get("contour1");
        g1->SetMarkerColor(63);
        TGraph *g2 = (TGraph *)f1->Get("center");
        g2->SetMarkerColor(63);
        mg->Add(g1);
        mg->Add(g2);
        centralitystr.Clear(); // clear the string centrality
        f1->Close();
    }

    mg->SetTitle("T_{kin}  in function of <#Beta_{T}> for " + name + " ; <#Beta_{T}>; T_{kin}(GeV)");
    mg->Draw("apl");
    c->Update();
    // mg->SetMinimum(0.02);
    // mg->SetMaximum(0.18);
    mg->GetXaxis()->SetLimits(0.3, 0.7);
    //Line for T_ch
    double_t T_ch = 0.136; 
    TLine *line = new TLine(0.3, T_ch, 0.7, T_ch);
    line->SetLineColor(kRed);
    line->SetLineStyle(10);
    line->SetLineWidth(2);
    line->Draw();
    //Legend T_ch
    TLatex *l1 = new TLatex();
    l1->SetTextSize(0.020);
    l1->SetTextColor(2);
    l1->DrawTextNDC(0.79, 0.65, "T_{ch}");
    c->Modified();
    c->Update();

    c->SaveAs("./histo/fit/" + name + "/contours.root");
}
