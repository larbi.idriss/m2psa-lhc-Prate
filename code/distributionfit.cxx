#include "distributionfit.h"

using namespace std;

void DrawLegend(TString name, Int_t id_dis, TF1 *f1, TGraphAsymmErrors *graph, TCanvas *c1, TH1 *hist)
{
    // Legend
    auto stats1 = (TPaveStats *)graph->GetListOfFunctions()->FindObject("stats");
    stats1->SetX1NDC(0.72);
    stats1->SetX2NDC(0.92);
    stats1->SetY1NDC(0.75);
    c1->Modified();
    gStyle->SetLegendTextSize(0.02); // Legend
    auto leg1 = new TLegend(0.7, 0.65, 0.95, 0.50);
    leg1->AddEntry(f1, GetdistName(id_dis) + " fit", "l");
    leg1->AddEntry(graph, "p_T distribtion of " + name, "l");
    leg1->Draw();
    // Intergral
    double errorhist;
    TString totIntegral, extIntegral, hisIntegral = "";
    // Extrpolated Integral
    extIntegral += f1->Integral(0, graph->GetPointX(0));
    extIntegral.Resize(4);
    cout << "ExtIntegral = " << f1->Integral(0, graph->GetPointX(0)) << "+/-" << f1->IntegralError(0, graph->GetPointX(0)) << endl;
    extIntegral += "+/-";
    extIntegral += f1->IntegralError(0, graph->GetPointX(0));
    // Integral of the histogram
    hisIntegral += hist->Integral("WIDTH");
    hisIntegral.Resize(4);
    cout << "HisIntegral= " << hist->IntegralAndError(0, hist->GetNbinsX() - 1, errorhist, "WIDTH") << "+/-" << errorhist << endl;
    hisIntegral += "+/-";
    hisIntegral += errorhist;
    // Total Integral
    totIntegral += f1->Integral(0, graph->GetPointX(0)) + hist->Integral(0, hist->GetNbinsX() - 1, "WIDTH");
    totIntegral.Resize(4);
    cout << "TotIntegral = " << f1->Integral(0, graph->GetPointX(0)) + hist->Integral(0, hist->GetNbinsX() - 1, "WIDTH") << "+/-"
         << f1->IntegralError(0, graph->GetPointX(0)) << "+/-" << errorhist << endl;
    totIntegral += "+/-";
    totIntegral += errorhist;
    totIntegral.Resize(11);
    totIntegral += "+/-";
    totIntegral += f1->IntegralError(0, graph->GetPointX(0));
    // TLatex used to display the result on the graph
    extIntegral.Resize(11);
    totIntegral.Resize(19);
    hisIntegral.Resize(11);
    TLatex *l1 = new TLatex(0.2, 0.45, "TotIntegral");
    TLatex *l2 = new TLatex(0.2, 0.65, "ExpIntegral");
    TLatex *l3 = new TLatex(0.2, 0.85, "HisIntegral");
    l1->SetTextSize(0.020);
    l2->SetTextSize(0.020);
    l3->SetTextSize(0.020);
    l1->DrawTextNDC(0.5, 0.25, "Total " + GetdistName(id_dis) + " Integral = " + totIntegral);
    l2->DrawTextNDC(0.5, 0.3, "Extrapolated " + GetdistName(id_dis) + " Integral = " + extIntegral);
    l3->DrawTextNDC(0.5, 0.35, "Histogram " + GetdistName(id_dis) + " Integral = " + hisIntegral);
    c1->Modified();
}

void FitRange(Int_t id_dis, TF1 *f1, TGraphAsymmErrors *graph, TCanvas *c1, Double_t fitmin, Double_t fitmax)
{
    f1->SetRange(0, 10);
    if (fitmin == 0 && fitmax == 0)
    {
        graph->Fit(GetdistName(id_dis), "EX0EM", "");
        TLatex *l = new TLatex(0.2, 0.45, "range");
        f1->Draw();
        l->SetTextSize(0.020);
        l->DrawTextNDC(0.5, 0.2, "No fitting range specified");
        c1->Update();
    }
    else
    {
        TString fitminstr, fitmaxstr = "";
        fitminstr += fitmin;
        fitmaxstr += fitmax;
        graph->Fit(GetdistName(id_dis), "EX0EM", "", fitmin, fitmax);
        f1->Draw();
        TLatex *l = new TLatex(0.2, 0.45, "range");
        l->SetTextSize(0.020);
        l->DrawTextNDC(0.5, 0.2, "Fitting range = [" + fitminstr + ";" + fitmaxstr + "]");
        c1->Update();
    }
}

void distributionfit(Int_t id_dis, TString fichier, int tableNumber, int centrality, TString name, Double_t fitmin, Double_t fitmax)
{
    // Converting cnetrality int into TString
    TString centralitystr = "";
    centralitystr += centrality;

    TGraphAsymmErrors *graph = (TGraphAsymmErrors *)GetGraph(fichier, tableNumber, centrality);
    graph->SetTitle("p_{T} distribution of " + name);
    graph->GetXaxis()->SetLimits(0, 10);
    graph->SetLineColor(65);

    // Draw
    TFile *file = new TFile("./histo/fit/" + name + "/" + centralitystr + "/fit" + GetdistName(id_dis) + name + "graph.root", "recreate");
    TCanvas *c1 = new TCanvas("distribution" + GetdistName(id_dis) + centralitystr, "Fit", 80, 80, 900, 900);
    c1->SetGrid();
    TH1 *hist = GetHist(fichier, tableNumber, centrality);

    switch (id_dis)
    {
    case 1:
    {
        gStyle->SetOptFit(0011);
        gStyle->SetOptFit();
        // Exponential distribution
        TF1 *f1 = new TF1(GetdistName(id_dis), DistrExp, 0, 20, 3); // 3 parameters
        f1->SetParameters(GetPartmass(fichier, tableNumber), 0.427, 74);
        f1->FixParameter(0, GetPartmass(fichier, tableNumber));
        f1->SetParNames("mass", "Temperature", "Yield");
        f1->SetLineColor(8); // green
        // Draw fit and print choosen range
        FitRange(id_dis, f1, graph, c1, fitmin, fitmax);
        // Draw graph
        graph->Draw("sames");
        c1->Update();
        // Legend and integral
        DrawLegend(name, id_dis, f1, graph, c1, hist);
        // Contour
        contourfit(f1, name, centralitystr, id_dis, 1, 2); // draw the contour
        // Printing
        file->cd();
        f1->Write("Exp");
    }
    break;
    case 2:
    {
        gStyle->SetOptFit(0011);
        gStyle->SetOptFit();
        // Boltzmann distribution
        TF1 *f2 = new TF1(GetdistName(id_dis), DistrBoltzmann, 0, 20, 3); // 3 parameters
        f2->SetParameters(GetPartmass(fichier, tableNumber), 0.1, 1);
        f2->SetParLimits(1, 0, 0.9);
        f2->FixParameter(0, GetPartmass(fichier, tableNumber));
        f2->SetParNames("mass", "Temperature", "Normalisation");
        f2->SetLineColor(4); // bleu
        // Draw fit and print choosen range
        FitRange(id_dis, f2, graph, c1, fitmin, fitmax);
        // Draw graph
        graph->Draw("sames");
        c1->Update();
        // Legend and integral
        DrawLegend(name, id_dis, f2, graph, c1, hist);
        // Contour
        contourfit(f2, name, centralitystr, id_dis, 1, 2); // draw the contour
        // Printing
        f2->Write("Boltzmann");
    }
    break;
    case 3:
    {
        gStyle->SetOptFit(0011);
        gStyle->SetOptFit();
        // Levy distribution
        TF1 *f3 = new TF1(GetdistName(id_dis), DistrLevy, 0, 20, 4);
        f3->SetParameters(GetPartmass(fichier, tableNumber), 0.164, 74, 4);
        f3->FixParameter(0, GetPartmass(fichier, tableNumber));
        // f3->SetParLimits(1, 0, 0.5);
        f3->SetParNames("mass", "Temperature", "Yield ", "Power index");
        f3->SetLineColor(6); // magenta
        // Draw fit and print choosen range
        FitRange(id_dis, f3, graph, c1, fitmin, fitmax);
        // Draw graph
        graph->Draw("sames");
        c1->Update();
        // Legend and integral
        DrawLegend(name, id_dis, f3, graph, c1, hist);
        // Contour
        contourfit(f3, name, centralitystr, id_dis, 1, 2); // draw the contour
        // Printing
        file->cd();
        f3->Write("Levy");
    }
    break;
    case 4:
    {
        gStyle->SetOptFit(0011);
        gStyle->SetOptFit();
        // Power distribution
        TF1 *f4 = new TF1(GetdistName(id_dis), DistrPow, 0, 20, 3);
        f4->SetParameters(1.44, 74, 5);
        f4->SetParNames("average momentum", "Yield ", "Power index");
        f4->SetLineColor(9); // violet
        // Draw fit and print choosen range
        FitRange(id_dis, f4, graph, c1, fitmin, fitmax);
        // Draw graph
        graph->Draw("sames");
        c1->Update();
        // Legend and integral
        DrawLegend(name, id_dis, f4, graph, c1, hist);
        // Contour
        contourfit(f4, name, centralitystr, id_dis, 0, 1); // draw the contour
        // Printing
        file->cd();
        f4->Write("Power");
    }
    break;
    case 5:
    {
        gStyle->SetOptFit(0011);
        gStyle->SetOptFit();
        TF1 *f5 = new TF1(GetdistName(id_dis), DistBlastwave, 0, 20, 6);
        // Parameters
        f5->SetParameters(GetPartmass(fichier, tableNumber), 918500, 10, 0.7, 0.6, 0.09);
        f5->FixParameter(0, GetPartmass(fichier, tableNumber));
        f5->FixParameter(1,  GetNormBW(tableNumber,centrality) ); //N
        f5->FixParameter(2, GetRadius(centrality)); // radius fixed in fm
        f5->SetParLimits(3, 0.7, 2.4);              // n
        f5->SetParLimits(4, 0, 0.8);                // beta
        f5->SetParLimits(5, 0.07, 0.18);             // T
        f5->SetParNames("mass", "Normalisation", "Radius", "Order", "<#Beta_{T}>", "Temperature");
        f5->SetLineColor(96); // orange
        // Draw fit and print choosen range
        FitRange(id_dis, f5, graph, c1, fitmin, fitmax); // 0.2;1.2
        // Draw graph
        graph->Draw("sames");
        c1->Update();
        // Legend and integral
        DrawLegend(name, id_dis, f5, graph, c1, hist);
        // Contour
        contourfit(f5, name, centralitystr, id_dis, 4, 5); // draw the contour
        // Printing
        file->cd();
        f5->Write("BW");
        ;
        break;
    }
    default:
        // Exponential distribution
        TF1 *f1 = new TF1("Exp", DistrExp, 0, 10, 3); // 3 parameters
        // f1->SetMaximum(45);
        f1->SetTitle("fit of p_{T} distribtion of " + name);
        f1->SetParameters(GetPartmass(fichier, tableNumber), 0.427, 74);
        f1->FixParameter(0, GetPartmass(fichier, tableNumber));
        f1->SetParNames("mass", "Temperature", "Yield");
        f1->SetLineColor(2); // rouge
        f1->SetRange(0, 10);
        graph->Fit("Exp", "EX0", "", 0, 1.5);
        f1->Draw();
        graph->Draw("sames");
        c1->Update();
        // Boltzmann distribution
        TF1 *f2 = new TF1("Boltzmann", DistrBoltzmann, 0, 10, 3); // 3 parameters
        f2->SetParameters(GetPartmass(fichier, tableNumber), 0.1, 1);
        f2->FixParameter(0, GetPartmass(fichier, tableNumber));
        f2->SetParNames("mass", "Temperature", "Normalisation");
        f2->SetLineColor(4); // bleu
        f2->SetRange(0, 10);
        graph->Fit("Boltzmann", "EX0", "", 0, 1.5);
        f2->Draw("sames");
        graph->Draw("sames");
        c1->Update();
        // Levy distribution
        TF1 *f3 = new TF1("Levy", DistrLevy, 0, 10, 4);
        f3->SetParameters(GetPartmass(fichier, tableNumber), 0.164, 74, 4);
        f3->FixParameter(0, GetPartmass(fichier, tableNumber));
        f3->SetParLimits(1, 0, 0.9);
        f3->SetParNames("mass", "Temperature", "Yield ", "Power index");
        f3->SetLineColor(6); // magenta
        f3->SetRange(0, 10);
        graph->Fit("Levy", "EX0", "", 1, 4);
        f3->Draw("sames");
        graph->Draw("sames");
        c1->Update();
        // Power distribution
        TF1 *f4 = new TF1("Power", DistrPow, 0.5, 10, 3);
        f4->SetParameters(1.44, 74, 5);
        f4->SetParLimits(2, 0, 100);
        f4->SetParLimits(1, 0, 100);
        f4->SetParNames("average momentum", "Yield ", "Power index");
        f4->SetLineColor(9); // violet
        f4->SetRange(0, 10);
        graph->Fit("Power", "EX0", "", 0.4, 4);
        f4->Draw("sames");
        graph->Draw("sames");
        c1->Update();

        break;
    }
    c1->SaveAs("./histo/fit/" + name + "/" + centralitystr + "/fit" + GetdistName(id_dis) + name + ".root");
    file->Close();
}
