#include "combinedfit.h"

using namespace std;

/*Parameters of BW
    Double_t mass = par[0];   //mass [GeV/c] Not shared and fixed
    Double_t N = par[1];      //Normalisation Not shared
    Double_t R = par[2];      //Radius of the fireball [fm] //shared and fixed
    Double_t n = par[3];      //Order //shared
    Double_t Beta_T = par[4]; //Mean radial expension velocity //shared
    Double_t T = par[5];      //Temperature [GeV/c] //shared
*/
// definition of shared parameter

// BW protons
int iparp[6] = {
    0, // mass [GeV/c] Not shared and fixed
    1, // Normalisation Not shared
    2, // Radius of the fireball [fm] //shared and fixed
    3, // Order //shared
    4, // Mean radial expension velocity //shared
    5  // Temperature [GeV/c] //shared
};

// BW pions
int iparpions[6] = {
    6, // mass [GeV/c] Not shared and fixed
    7, // Normalisation Not shared
    2, // Radius of the fireball [fm] //shared and fixed
    3, // Order //shared
    4, // Mean radial expension velocity //shared
    5  // Temperature [GeV/c] //shared
};

// BW kaons
int iparkaons[6] = {
    8, // mass [GeV/c] Not shared and fixed
    9, // Normalisation Not shared
    2, // Radius of the fireball [fm] //shared and fixed
    3, // Order //shared
    4, // Mean radial expension velocity //shared
    5  // Temperature [GeV/c] //shared
};
// Create the GlobalCHi2 structure

struct GlobalChi2
{
    GlobalChi2(ROOT::Math::IMultiGenFunction &f1, ROOT::Math::IMultiGenFunction &f2, ROOT::Math::IMultiGenFunction &f3) : fChi2_1(&f1), fChi2_2(&f2), fChi2_3(&f3) {}

    double operator()(const double *par) const
    {
        double p1[6];
        for (int i = 0; i < 6; ++i)
            p1[i] = par[iparp[i]];

        double p2[6];
        for (int i = 0; i < 6; ++i)
            p2[i] = par[iparpions[i]];

        double p3[6];
        for (int i = 0; i < 6; ++i)
            p3[i] = par[iparkaons[i]];

        return (*fChi2_1)(p1) + (*fChi2_2)(p2) + (*fChi2_3)(p3);
    }

    const ROOT::Math::IMultiGenFunction *fChi2_1;
    const ROOT::Math::IMultiGenFunction *fChi2_2;
    const ROOT::Math::IMultiGenFunction *fChi2_3;
};

void combinedFit(int centrality)
{
    TString fichier = "data/HEPData-1569102768-v1-root.root";
    TString centralitystr = "";
    centralitystr += centrality;

    // graph to fit
    TGraphAsymmErrors *graphp = (TGraphAsymmErrors *)GetGraph(fichier, 5, centrality);
    TGraphAsymmErrors *graphpions = (TGraphAsymmErrors *)GetGraph(fichier, 1, centrality);
    TGraphAsymmErrors *graphkaons = (TGraphAsymmErrors *)GetGraph(fichier, 3, centrality);

    TF1 *fp = new TF1("BWprotons", DistBlastwave, 0, 20, 6); // protons
    // Parameters
    fp->SetParameters(GetPartmass(fichier, 5), 19000, 10.5, 0.7, 0.6, 0.09);
    fp->FixParameter(0, GetPartmass(fichier, 5)); // mass fixed
    fp->FixParameter(2, GetRadius(centrality));   // radius fixed in fm
    fp->SetParNames("mass", "Normalisation", "Radius", "Order", "<#Beta_{T}>", "Temperature");

    TF1 *fpions = new TF1("BWpions", DistBlastwave, 0, 20, 6); // pions
    // Parameters
    fpions->SetParameters(GetPartmass(fichier, 1), 19000, 10.5, 0.7, 0.6, 0.09);
    fpions->FixParameter(0, GetPartmass(fichier, 1)); // mass fixed
    fpions->FixParameter(2, GetRadius(centrality));   // radius fixed in fm
    fpions->SetParNames("mass", "Normalisation", "Radius", "Order", "<#Beta_{T}>", "Temperature");

    TF1 *fkaons = new TF1("BWkaons", DistBlastwave, 0, 20, 6); // pions
    // Parameters
    fkaons->SetParameters(GetPartmass(fichier, 3), 19000, 10.5, 0.7, 0.6, 0.09);
    fkaons->FixParameter(0, GetPartmass(fichier, 3)); // mass fixed
    fkaons->FixParameter(2, GetRadius(centrality));   // radius fixed in fm
    fkaons->SetParNames("mass", "Normalisation", "Radius", "Order", "<#Beta_{T}>", "Temperature");

    // perform now global fit

    ROOT::Math::WrappedMultiTF1 wfp(*fp, 1);
    ROOT::Math::WrappedMultiTF1 wfpions(*fpions, 1);
    ROOT::Math::WrappedMultiTF1 wfkaons(*fkaons, 1);

    // protons
    ROOT::Fit::DataOptions opt;
    ROOT::Fit::DataRange rangep;
    // set the data range
    rangep.SetRange(0.3, 3);
    ROOT::Fit::BinData datap(opt, rangep);
    ROOT::Fit::FillData(datap, graphp);

    // pions
    ROOT::Fit::DataRange rangepions;
    rangepions.SetRange(0.5, 1.5);
    ROOT::Fit::BinData datapions(opt, rangepions);
    ROOT::Fit::FillData(datapions, graphpions);

    // kaons
    ROOT::Fit::DataRange rangekaons;
    rangekaons.SetRange(0.2, 1.5);
    ROOT::Fit::BinData datakaons(opt, rangekaons);
    ROOT::Fit::FillData(datakaons, graphkaons);

    ///

    ROOT::Fit::Chi2Function chi2_p(datap, wfp);
    ROOT::Fit::Chi2Function chi2_pions(datapions, wfpions);
    ROOT::Fit::Chi2Function chi2_kaons(datakaons, wfkaons);

    GlobalChi2 globalChi2(chi2_p, chi2_pions, chi2_kaons);

    ROOT::Fit::Fitter fitter;

    const int Npar = 10;
    double par0[Npar] = { GetPartmass(fichier, 5), 900000, GetRadius(centrality), 0.7, 0.6, 0.09, GetPartmass(fichier, 1), 900000, GetPartmass(fichier, 3), 900000};

    // create before the parameter settings in order to fix or set range on them
    fitter.Config().SetParamsSettings(10, par0);
    // fix parameter
    fitter.Config().ParSettings(0).Fix(); // mass proton
    fitter.Config().ParSettings(2).Fix(); // Radius
    fitter.Config().ParSettings(6).Fix(); // mass pion
    fitter.Config().ParSettings(8).Fix(); // kaons
    // set limits on the third and 4-th parameter
    fitter.Config().ParSettings(1).SetLimits(300, 700000); // N1
    fitter.Config().ParSettings(3).SetLimits(0.7, 2.4);       // n
    fitter.Config().ParSettings(4).SetLimits(0, 0.8);       // Beta
    fitter.Config().ParSettings(5).SetLimits(0.07, 0.18); // T
    fitter.Config().ParSettings(7).SetLimits(100, 700000); // N2
    fitter.Config().ParSettings(9).SetLimits(100, 700000); // N3

    fitter.Config().MinimizerOptions().SetPrintLevel(0);
    fitter.Config().SetMinimizer("Minuit", "Migrad");

    // fit FCN function directly
    // (specify optionally data size and flag to indicate that is a chi2 fit)
    fitter.FitFCN(10, globalChi2, 0, datap.Size() + datapions.Size() + datakaons.Size(), true);
    ROOT::Fit::FitResult result = fitter.Result();
    result.Print(std::cout);

    TCanvas *c1 = new TCanvas("Simfit", "Simultaneous fit of 3 histograms", 10, 10, 700, 700);
    c1->Divide(2, 2);
    c1->cd(1);
    gStyle->SetOptFit(1111);

    fp->SetFitResult(result, iparp);
    fp->SetRange(0, 10);
    fp->SetLineColor(kBlue);
    graphp->GetListOfFunctions()->Add(fp);
    graphp->SetTitle("p_{T} distribution ");
    graphp->GetXaxis()->SetLimits(0, 6);
    graphp->SetLineColor(65);
    graphp->Draw();
    c1->Update();

    c1->cd(2);
    fpions->SetFitResult(result, iparpions);
    fpions->SetRange(0, 10);
    fpions->SetLineColor(kRed);
    graphpions->GetListOfFunctions()->Add(fpions);
    graphpions->SetTitle("p_{T} distribution ");
    graphpions->GetXaxis()->SetLimits(0, 6);
    graphpions->SetLineColor(65);
    graphpions->Draw();
    c1->Update();

    c1->cd(3);
    fkaons->SetFitResult(result, iparkaons);
    fkaons->SetRange(0, 10);
    fkaons->SetLineColor(kGreen);
    graphkaons->GetListOfFunctions()->Add(fkaons);
    graphkaons->SetTitle("p_{T} distribution ");
    graphkaons->GetXaxis()->SetLimits(0, 6);
    graphkaons->SetLineColor(65);
    graphkaons->Draw();
    c1->Update();


    c1->SaveAs("./histo/fit/combinedfit/combinedfit"+centralitystr+".root");
}
