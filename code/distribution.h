//O.Poncet and I.Larbi 
//__________________________________________________________________________
// Function to fit the data in distibutionfit 
//
#ifndef DISTRIBUTION
#define DISTRIBUTION

#include <TROOT.h>
#include <TObject.h>
#include <TMath.h>
#include <TString.h>
#include <TF1.h>

Double_t DistrExp(Double_t *x, Double_t *par); //Exponential distribution id_dis=1
Double_t DistrBoltzmann(Double_t *x, Double_t *par); //Boltzmann distribution id_dis=2
Double_t DistrLevy(Double_t *x, Double_t *par); //Levy Tsallis distribution id_dis=3
Double_t DistrPow(Double_t *x, Double_t *par); //Power distribution id_dis=4
Double_t BWintegrand(Double_t *r, Double_t *par); //Blast wave integrand
Double_t DistBlastwave(Double_t *x, Double_t *par); ////Exponential distribution id_dis=5
TString GetdistName(Int_t id_dis); //Get distibution name using id_dis 

#endif
