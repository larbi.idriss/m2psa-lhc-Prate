//O.Poncet and I.Larbi 
//__________________________________________________________________________
// Function to get different information concerning data and fit 
//
#ifndef GET
#define GET
#include <TROOT.h>
#include <TObject.h>
#include <TMath.h>
#include <TString.h>
#include <TFile.h>
#include <TH1.h>
#include <TGraph.h>
#include <TF1.h>
#include <TGraphAsymmErrors.h>
#include "distribution.h"


TH1 *GetHist(TString fichier, int tableNumber, int centrality);//Build and return one single histogram containing the dN/dpt with its total errors from three histograms in the root file
TGraph *GetGraph(TString fichier, int tableNumber, int centrality);//Get the TGraphAsymmErrors from one root file of data for a specific centrality and specie
Double_t GetPartmass(TString fichier, int tableNumber); //Get the mass of the produced hadron/meson to fit  
Double_t GetNormBW( int tableNumber, int centrality); //Get the normalisation of BW using the parameters of te litterature 
Double_t GetRadius(int centrality); //Get the radius of the fireball using radius form the litterature

#endif
