//O.Poncet and I.Larbi 
//__________________________________________________________________________
// Function to fit the data for a given centrality and range draw the legend 
//
#ifndef DISTRIBUTION_FIT
#define DISTRIBUTION_FIT
#include <TROOT.h>
#include <TObject.h>
#include <TMath.h>
#include <TString.h>
#include <TGraphAsymmErrors.h>
#include <TCanvas.h>
#include <TF1.h>
#include <TStyle.h>
#include <TPaveStats.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TMinuit.h>
#include <TVirtualFitter.h>
#include "get.h"
#include "distribution.h"
#include "contourfit.h"

void DrawLegend(TString name, Int_t id_dis, TF1 *f1, TGraphAsymmErrors *graph, TCanvas *c1, TH1* hist);//Display the result of fitting on the graph and on the standard input by making the integration of the distribution and of the histogram of data
void FitRange(Int_t id_dis, TF1 *f1, TGraphAsymmErrors *graph, TCanvas *c1, Double_t fitmin, Double_t fitmax);//Function perfoming the fit in the range [fitmin;fitmax] and displaying the range of fit on the graph of results
void distributionfit(Int_t id_dis, TString fichier, int tableNumber, int centrality, TString name, Double_t fitmin, Double_t fitmax);//Main function allowing the choice of the fitting distribution id_dis from 1 to 5 with a specific dataset (fichier,tableNumber,centrality) in the range [fitmin;fitmax] the function build also the contours for the distribution between two parameters the temperature and the yield, the temperature and <beta_t> according to the chosen distribution

#endif
