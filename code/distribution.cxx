
#include "distribution.h"

using namespace std;
using namespace TMath;

Double_t DistrExp(Double_t *x, Double_t *par) //id_dis = 1
{
    //works for low p_t
    // x[0] : p_T
    // T : temperature
    Double_t mass = par[0]; //mass [GeV/c]
    Double_t T = par[1];    //Temperature [GeV/c]
    Double_t N = par[2];    //Yield dN/dy

    Double_t m_T = TMath::Sqrt(x[0] * x[0] + pow(mass, 2)); //transverse mass

    return N * (x[0] / (T * (mass + T))) * exp((-m_T + mass) / T);
}

Double_t DistrBoltzmann(Double_t *x, Double_t *par) //id_dis = 2
{

    Double_t mass = par[0]; //mass [GeV/c]
    Double_t T = par[1];    //Temperature [GeV/c]
    Double_t C = par[2];    //Normalisation factor

    Double_t m_T = TMath::Sqrt(x[0] * x[0] + pow(mass, 2)); //transverse mass

    return C * m_T * x[0] * exp(-(m_T - mass) / T);
}

Double_t DistrLevy(Double_t *x, Double_t *par) //id_dis = 3
{
    Double_t mass = par[0]; //mass [GeV/c]
    Double_t T = par[1];    //Temperature [GeV/c]
    Double_t N = par[2];    //Yield dN/dy
    Double_t n = par[3];    //power index

    Double_t m_T = TMath::Sqrt(x[0] * x[0] + pow(mass, 2)); //transverse mass

    return N * x[0] * (n - 1) * (n - 2) / ((n * T) * (n * T + (mass * (n - 2)))) * pow(1 + (m_T - mass) / (n * T), -n);
}

Double_t DistrPow(Double_t *x, Double_t *par) //id_dis = 4
{
    //which one to use?

    Double_t p_T_avg = par[0]; //average momentum [GeV/c]
    Double_t N = par[1];       //Yield dN/dy
    Double_t n = par[2];       //power index

    return N * 4 * (n - 1) * (n - 2) * x[0] / ((n - 3) * (n - 3) * p_T_avg * p_T_avg) * pow(1 + x[0] / (p_T_avg * (n - 3) * 0.5), -n);
}

Double_t BWintegrand(Double_t *r, Double_t *par)
{
    Double_t mass = par[0];   //mass [GeV/c]
    Double_t pt = par[1];     //p_T [GeV/c]
    Double_t R = par[2];      //Radius of the fireball [fm]
    Double_t n = par[3];      //Order
    Double_t Beta_T = par[4]; //Mean radial expension velocity
    Double_t T = par[5];      //Temperature [GeV/c]

    Double_t m_T = Sqrt(pt * pt + mass * mass); //transverse mass
    Double_t rho = ATanH(Power(r[0] / R, n) * Beta_T * 0.5 * (2+n)); //Beta_s = Beta_T * 0.5 * (2+n) (see report)

    return BesselI0(pt * SinH(rho) / T) * BesselK1(m_T * CosH(rho) / T) * r[0];
}

Double_t DistBlastwave(Double_t *x, Double_t *par) //id_dis=5
{
    Double_t mass = par[0];   //mass [GeV/c]
    Double_t N = par[1];      //Normalisation
    Double_t R = par[2];      //Radius of the fireball [fm]
    Double_t n = par[3];      //Order
    Double_t Beta_T = par[4]; //Mean radial expension velocity 
    Double_t T = par[5];      //Temperature [GeV/c]

    Double_t m_T = Sqrt(x[0] * x[0] + mass * mass); //transverse mass

    TF1 *integrand = new TF1("integrand", BWintegrand, 0, R, 6);
    integrand->SetParameters(mass, x[0], R, n, Beta_T, T);
    //integrand->FixParameter(1,x[0]);

    Double_t integral = integrand->Integral(0, R, 1.e-6);

    delete integrand;
    
    return N * m_T * x[0] * integral;
}

TString GetdistName(Int_t id_dis)
{
    TString name;

    switch (id_dis)
    {
    case 1:
        name = name.Append("DistrExp");
        break;
    case 2:
        name = name.Append("DistrBoltzmann");
        break;
    case 3:
        name = name.Append("DistrLevy");
        break;
    case 4:
        name = name.Append("DistrPow");
        break;
    case 5:
        name = name.Append("BW");
        break;
    default:
        name = name.Append("all");
        break;
    }

    return name;
}
