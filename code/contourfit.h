//O.Poncet and I.Larbi 
//__________________________________________________________________________
// Function to plot the contours of the fit obtained in distibutionfit 
//
#ifndef CONTOUR_FIT
#define CONTOUR_FIT
#include <TROOT.h>
#include <TObject.h>
#include <TMath.h>
#include <TString.h>
#include <TGraphAsymmErrors.h>
#include <TCanvas.h>
#include <TF1.h>
#include <TStyle.h>
#include <TPaveStats.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TMinuit.h>
#include <TVirtualFitter.h>
#include <TMultiGraph.h>
#include <TGraph.h>
#include <TLine.h>
#include "distributionfit.h"

void contourfit(TF1 *f ,TString name, TString centralitystr, Int_t id_dis, Int_t par1, Int_t par2);//Build the contour between parameters par1 and par2 for a given centrality and distribution for the last fit realized.
void contours(TString name, Int_t centralitymin, Int_t centralitymax);//Take several contours built by contourfit (for several centrality) and draw all of them on a same graph to see how the centrality affects the kinetic temperature and the <beta_t> ot the kinetic temperature and the yield 

#endif
