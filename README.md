# M2PSA-LHC-Productionrate

## Description
This project implements ROOT routines in C++ to study the integrated production rates for different hadrons obtained in proton-proton (pp) and lead-lead (Pb–Pb) collisions in ALICE experement at sqrt{s_{NN}} = 5.02 TeV.<br>
The data used for ppbar,pions and kaons are from <https://www.hepdata.net/record/sandbox/1569102768> and the phi data are taken from <https://www.hepdata.net/record/ins1762368>.<br>
The goal of this study is to compare thermal and hydrodynamic models to fit the data and to be able to extrapolate the data at low momentum where the detector is blinded. These fits give access to important quantities such as temperature and radial expansion velocity and allows to calculate the production rates over a range from 0 to 20 GeV/c². 

## Code Structure
Each source file (.cxx) has its header file (.h) except the main.<br>

First, the file *get.h* groups all functions associated to the extraction of the data from the root file(s) downable with the links above.<br>
Then, the file *distribution.h* groups all distributions used to fit the data such as Blast-Wave,exponential,Boltzmann,Lévy-Tsallis and Power.<br>
The file *distributionfit.h* contains the function use to fit any kind of collisions in every centrality with a specific distribution id from 1 to 5, the corresponding name of distribution can be access by function GetdistName() in *distribution.h*.<br> 
The file *contourfit.h* contains the function **contourfit()** to make the contour at 2 sigma of two parameters of a fit with a specific centrality, distribution and species saved in a Tgraph and a function **contours()** which creates a multigraph, containing contours for different centralities, which can be visualized with ROOT later.<br> 
The project also contains the file *combinedfit.h* with a function to make a multifit over different species in a same time to have more constraints on the common parameters like the temperature.<br>

To finish the *main.cxx* contains all the different steps done during the project commented with now the calculus of contours for centrality minimal to maximal for PbPb collisions producing ppbar hadrons.

## Installation
First, you need to **install the last version of ROOT 6.24/06** and source the file */root/bin/thisroot.sh*. For more details for ROOT installation follow root installation from pre-compiled binary distribution at this link <https://root.cern/install/#download-a-pre-compiled-binary-distribution>.<br>
To make the installation you just have to download the code directory with .cxx and .h files and then to run the command of installation in the **parent directory** written below in Usage section. 

## Usage
g++ code/*.cxx $(root-config --glibs --cflags --libs) -lMinuit -o m2psa-lhc-Prate <br>
./m2psa-lhc-Prate

## Results
To see the different fits and contours results, you can run root and use TBrowser tb to browse the different graphs created by the executable m2psa-lhc-Prate. The results are organised in a tree:
- histo
    - fit
        - PbPb_kaons
            - Centrality_1
                - Contour
                    - contourBW
                    - contourExp
                    - ...
                - Botlzmann_dist
                - Levy_dist
                - Power_dist
                - Exp_dist
                - Blast-Wave_dist
            - Centrality_2
            - ...
            - Centrality_10
        - PbPb_ppbar
        - PbPb_phi
        - PbPb_pions
        - pp_phi
        - pp_ppbar
        - Combinedfit
            - Centrality_1
            - ...
            - Centrality_10
        

## Support
For more precision please contact <oceane.poncet@etu.unistra.fr> or <idriss.larbi@etu.unistra.fr>.

## Authors and acknowledgment
This project was conducted by Oceane Poncet and Idriss Larbi and with the advices of Boris Hippolyte and Romain Schotter who we would particularly like to thank for their help throughout this project.



